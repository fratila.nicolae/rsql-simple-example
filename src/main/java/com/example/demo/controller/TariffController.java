package com.example.demo.controller;

import com.example.demo.model.Tariff;
import com.example.demo.service.TariffService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class TariffController {
  @Autowired private TariffService service;

  @GetMapping()
  public List<Tariff> getTariffs(@PathVariable(required = false) String search, @PathVariable(required = false) String order) {
    if (search == null) search = "";
    if (order == null) return service.getTariff(search);
    return service.getTariffSorted(search, order);
  }

  @PostMapping
    public Tariff addTariff(@RequestBody Tariff tariff) {
      return service.saveTariff(tariff);
  }
}
