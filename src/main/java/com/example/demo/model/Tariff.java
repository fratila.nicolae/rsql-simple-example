package com.example.demo.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Tariff {

  @Id private Long id;
  private String cpoId;
  private String country;
  private String application;
  private Date validFrom;
  private Date validTo;
}
