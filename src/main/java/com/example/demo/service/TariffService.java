package com.example.demo.service;

import com.example.demo.model.Tariff;
import com.example.demo.repository.TariffRepo;
import cz.jirutka.rsql.hibernate.RSQL2CriteriaConverter;
import cz.jirutka.rsql.hibernate.RSQL2HibernateFactory;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.DetachedCriteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TariffService {
  @Autowired private TariffRepo repo;

  @Autowired private SessionFactory sessionFactory;

  public List<Tariff> getTariff(String query) {
    // what we need from Hibernate

    Session session;
    try {
      session = sessionFactory.getCurrentSession();
    } catch (HibernateException e) {
      session = sessionFactory.openSession();
    }

    // setup factory
    RSQL2HibernateFactory factory = RSQL2HibernateFactory.getInstance();
    factory.setSessionFactory(sessionFactory);
    // create converter (may be done e.g. in Spring’s context)
    RSQL2CriteriaConverter converter = factory.createConverter();
    // parse RSQL and create detached criteria for specified entity class
    DetachedCriteria detached = converter.createCriteria("country==2*;cpoId==3", Tariff.class);
    // connect it with current Hibernate Session
    Criteria criteria = detached.getExecutableCriteria(session);
    criteria.setFirstResult(10);
    criteria.setMaxResults(10);
//    criteria.add(Criterion)
    // execute query and get result
    List<Tariff> result = criteria.list();
    return result;
  }

  public List<Tariff> getTariffSorted(String query, String sort) {
    // what we need from Hibernate
    Session session = sessionFactory.getCurrentSession();

    if (session == null) {
      sessionFactory.openSession();
    }
    // setup factory
    RSQL2HibernateFactory factory = RSQL2HibernateFactory.getInstance();
    factory.setSessionFactory(sessionFactory);
    // create converter (may be done e.g. in Spring’s context)
    RSQL2CriteriaConverter converter = factory.createConverter();
    // parse RSQL and create detached criteria for specified entity class
    DetachedCriteria detached = converter.createCriteria(query, sort, true, Tariff.class);
    // connect it with current Hibernate Session
    Criteria criteria = detached.getExecutableCriteria(session);
    // execute query and get result
    List<Tariff> result = criteria.list();
    return result;
  }

    public Tariff saveTariff(Tariff tariff) {
        return repo.save(tariff);
    }
}
